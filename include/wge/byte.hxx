#ifndef WGE_BYTE_HXX
#define WGE_BYTE_HXX

#include <nonstd/byte.hpp>

namespace wge
{
  using nonstd::byte;

  using nonstd::to_byte;
  using nonstd::to_integer;
} // namespace wge

#endif // WGE_BYTE_HXX