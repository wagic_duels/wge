#ifndef WGE_MATH_HXX
#define WGE_MATH_HXX

#include <cmath>
#include <type_traits>

namespace wge
{

  template <typename T, class = typename std::enable_if<std::is_integral<T>::value>::type>
  inline T nearest_superior_power_of_2(T n)
  {
    auto value = std::pow(2, std::ceil(std::log(n) / std::log(2)));
    return static_cast<T>(value);
  }

} // namespace wge
#endif /* WGE_MATH_HXX */
