#ifndef WGE_STRING_HXX
#define WGE_STRING_HXX

#include <algorithm>
#include <cctype>
#include <string>

#if defined(PSP)

#  include <sstream>

namespace wge
{
  template <typename T>
  std::string to_string(const T& n)
  {
    std::ostringstream stm;
    stm << n;
    return stm.str();
  }
} // namespace wge
#else
namespace wge
{
  using std::to_string;
}
#endif

namespace wge
{
  inline std::string& ltrim(std::string& s) noexcept
  {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) { return !std::isspace(ch); }));
    return s;
  }

  inline std::string& rtrim(std::string& s) noexcept
  {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) { return !std::isspace(ch); }).base(), s.end());
    return s;
  }

  inline std::string& trim(std::string& s) noexcept { return ltrim(rtrim(s)); }

  inline std::string ltrim_copy(std::string s) noexcept { return ltrim(s); }
  inline std::string rtrim_copy(std::string s) noexcept { return rtrim(s); }
  inline std::string trim_copy(std::string s) noexcept { return trim(s); }

  inline bool contains(const std::string& lhs, const std::string& rhs) noexcept { return lhs.find(rhs) != std::string::npos; }

} // namespace wge

#endif // WGE_STRING_HXX