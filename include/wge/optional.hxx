#ifndef WGE_OPTIONAL_HXX
#define WGE_OPTIONAL_HXX

#include <nonstd/optional.hpp>

namespace wge
{
  using nonstd::bad_optional_access;
  using nonstd::optional;

  using nonstd::operator==;
  using nonstd::operator!=;
  using nonstd::operator<;
  using nonstd::operator<=;
  using nonstd::operator>;
  using nonstd::operator>=;
  using nonstd::make_optional;
  using nonstd::swap;
} // namespace wge

#endif // WGE_OPTIONAL_HXX