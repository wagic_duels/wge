#include <wge/string.hxx>

#include <catch.hpp>

TEST_CASE("test string trim functions", "[wge][string]")
{
  SECTION("trim is not needed")
  {
    const std::string test_string("this is a test string");
    std::string string_under_test(test_string);
    REQUIRE(test_string == wge::trim(string_under_test));
  }

  SECTION("right trim")
  {
    const std::string test_string("this is a test string ");
    std::string string_under_test(test_string);

    auto rtrim_copy = wge::rtrim_copy(string_under_test);
    auto trim_copy = wge::trim_copy(string_under_test);
    REQUIRE(test_string.size() == (trim_copy.size() + 1));
    REQUIRE(trim_copy == rtrim_copy);

    REQUIRE(trim_copy == wge::rtrim(string_under_test));
  }

  SECTION("left trim")
  {
    const std::string test_string(" this is a test string");
    std::string string_under_test(test_string);

    auto ltrim_copy = wge::ltrim_copy(string_under_test);
    auto trim_copy = wge::trim_copy(string_under_test);
    REQUIRE(test_string.size() == (trim_copy.size() + 1));
    REQUIRE(trim_copy == ltrim_copy);

    REQUIRE(trim_copy == wge::ltrim(string_under_test));
  }

  SECTION("trim with multiple characters")
  {
    const std::string test_string("  this is a test string  ");
    std::string string_under_test(test_string);

    auto trim_copy = wge::trim_copy(string_under_test);
    REQUIRE(test_string.size() == (trim_copy.size() + 4));

    REQUIRE(trim_copy == wge::trim(string_under_test));
  }
}
