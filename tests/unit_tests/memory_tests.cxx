#include <wge/memory.hxx>

#include <catch.hpp>

TEST_CASE("test owner type", "[wge][memory]")
{
  SECTION("test nullptr compare")
  {
    wge::owner<int*> ptr = nullptr;
    REQUIRE(ptr == nullptr);
  }

  SECTION("test pointer access")
  {
    struct util
    {
      static void increment(int* i) { *i += 1; }
    };

    wge::owner<int*> ptr = new int(42);
    REQUIRE(ptr != nullptr);
    REQUIRE(*ptr == 42);

    util::increment(ptr);
    REQUIRE(*ptr == 43);

    delete ptr;
  }
}
