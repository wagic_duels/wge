cmake_minimum_required(VERSION 3.1)

add_subdirectory(catch2)
add_subdirectory(nonstd)

########################################################################################
# TINYFORMAT
########################################################################################
add_library(tinyformat INTERFACE)
add_library(TFM::tinyformat ALIAS tinyformat)

set(TINYFORMT_INCLUDE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/tinyformat")
target_sources(tinyformat INTERFACE "${TINYFORMT_INCLUDE_DIR}/tinyformat.h")
target_include_directories(tinyformat INTERFACE "${TINYFORMT_INCLUDE_DIR}")